package websockethttp

import (
	"log"
	"net/http"
	"testing"
)

func TestServer(t *testing.T) {
	server := ServerNew("test-server")
	server.ShowPongLogs(true)
	server.SetAuthorizationFunc(func(request *http.Request) (string, bool) {
		name := request.URL.Query().Get("name")
		return name, len(name) > 2
	})
	server.RegisterProcessFunc("ServerTest", func(context *ServerContext) {
		log.Printf("收到请求信息: %v", context.Request)
		// 设置响应数据
		context.Response.Code = 0
		context.Response.Message = "ok"
		context.Response.Body = "你好，这是服务器！"
	})
	server.AddConnOpenEventFunc(func(channel *ClientConnChannel) {
		log.Printf("收到连接: %v", channel.ConnName)
		channel.SendMessage("ClientTest", nil, "Hi client, conn ok !", func(context *ServerContext) {
			log.Printf("收到响应信息: %v", context.Response.Body)
		})
	})
	_ = server.LauncherServer("/websockethttp", 8080)
}
