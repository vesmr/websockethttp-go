# websockethttp-go

### websockethttp 协议的 golang 支持

> [websockethttp](https://gitee.com/vesmr/websockethttp "https://gitee.com/vesmr/websockethttp")

### 使用方式

#### 使用 get 命令从仓库加载依赖

```go
    go get gitee.com/vesmr/websockethttp-go
```

#### 服务端使用 [[server_test.go](./server_test.go "server_test.go")]

```go
    server := ServerNew("test-server")
    server.ShowPongLogs(true)
    server.SetAuthorizationFunc(func(request *http.Request) (string, bool) {
        name := request.URL.Query().Get("name")
        return name, len(name) > 2
    })
	
    // 注册一个Process等待客户端访问
    server.RegisterProcessFunc("ServerTest", func(context *ServerContext) {
        log.Printf("收到请求信息: %v", context.Request)
        // 设置响应数据
        context.Response.Code = 0
        context.Response.Message = "ok"
        context.Response.Body = "你好，这是服务器！"
    })
	
    // 监听连接成功事件
    server.AddConnOpenEventFunc(func(channel *ClientConnChannel) {
        log.Printf("收到连接: %v", channel.ConnName)    // 如果有集群部署需求可以在这个函数中保存连接记录到 redis 等共享给集群
		
        // 给客户端推送消息
        channel.SendMessage("ClientTest", nil, "Hi client, conn ok !", func(context *ServerContext) {
            log.Printf("收到响应信息: %v", context.Response.Body)
        })
    })
    _ = server.LauncherServer("/websockethttp", 8080)
```

#### 客户端使用 [[client_test.go](./client_test.go "client_test.go")]

```go
    client, err := ClientNew("ws://127.0.0.1:8080/websockethttp?name=test", "test-client")
    if err != nil {
        log.Printf("连接错误: %v", err)
        return
    }
    client.ShowPongLogs(true)
    
    // 注册一个Process等待服务器推送
    client.RegisterProcessFunc("ClientTest", func(context *ClientContext) {
        log.Printf("收到请求信息: %v", context.Request)
        // 设置响应数据
        context.Response.Code = 0
        context.Response.Message = "ok"
        context.Response.Body = "你好，这是客户端！"
    })

    // 发送消息到服务器
    client.SendMessage("ServerTest", nil, "Hi, server !", func(context *ClientContext) {
        log.Printf("收到响应信息: %v", context.Response.Body)
    })
```

#### 注意事项

如果你的服务器部署时使用了nginx代理，请不要忘记了配置nginx的websocket的代理支持。

```text
    map $http_upgrade $connection_upgrade { 
        default upgrade; 
        '' close; 
    }
```