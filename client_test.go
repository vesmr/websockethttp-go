package websockethttp

import (
	"log"
	"testing"
)

func TestClient(t *testing.T) {
	client, err := ClientNew("ws://127.0.0.1:8080/websockethttp?name=test", "test-client")
	if err != nil {
		log.Printf("连接错误: %v", err)
		return
	}
	client.ShowPongLogs(true)

	client.RegisterProcessFunc("ClientTest", func(context *ClientContext) {
		log.Printf("收到请求信息: %v", context.Request)
		// 设置响应数据
		context.Response.Code = 0
		context.Response.Message = "ok"
		context.Response.Body = "你好，这是客户端！"
	})

	client.SendMessage("ServerTest", nil, "Hi, server !", func(context *ClientContext) {
		log.Printf("收到响应信息: %v", context.Response.Body)
	})

	// 避免线程终止
	_ = <-make(chan int)
}
